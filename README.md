# FluxUtils

| Environment | CI Status |  Coverage |
|:-:|:-:|:-:|
| Production | [![pipeline status](https://gitlab.com/uplug/flux/helpers/flux_utils/badges/master/build.svg)](https://gitlab.com/uplug/flux/helpers/flux_utils/commits/master) | [![coverage report](https://gitlab.com/uplug/flux/helpers/flux_utils/badges/master/coverage.svg)](https://gitlab.com/uplug/flux/helpers/flux_utils/commits/master) |
| Development | [![pipeline status](https://gitlab.com/uplug/flux/helpers/flux_utils/badges/dev/build.svg)](https://gitlab.com/uplug/flux/helpers/flux_utils/commits/dev) | [![coverage report](https://gitlab.com/uplug/flux/helpers/flux_utils/badges/dev/coverage.svg)](https://gitlab.com/uplug/flux/helpers/flux_utils/commits/dev) |

## Environment Variables

### Logger

* `HANDLE_OTP_REPORTS`

    * **Defined on runtime?**: `false`
    * **Default**: `false`
    * **Options**: `true`, `false`
    * **Description**: Activate/deactivate OTP reports (log).
* `LOGGER_LEVEL`

    * **Defined on runtime?**: `false`
    * **Default**: `info`,
    * **Options**: `debug`, `info`, `warn`, `error`
    * **Description**: Logger verbosity.

### Utils

* `FLUX_ENV`

    * **Defined on runtime?**: `true`
    * **Default**: `dev`
    * **Options**: `dev`, `prod`, `test`
    * **Description**: Definition of environment context for flux services.
