use Mix.Config

config :logger,
  handle_otp_reports: String.to_existing_atom(System.get_env("HANDLE_OTP_REPORTS") || "false")

config :logger, :console,
  level: String.to_atom(System.get_env("LOGGER_LEVEL") || "info")
