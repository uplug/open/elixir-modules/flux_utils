defmodule FluxUtils.Constants do
  @callback constants(environment :: atom) :: %{optional(atom) => {any, atom}}

  defmacro __using__(_opts) do
    quote do
      def get(key) do
        case FluxUtils.Env.get_env(:flux_env, :dev, :atom) |> constants() |> Map.get(key) do
          {default, type} -> FluxUtils.Env.get_env(key, default, type)
          _ -> nil
        end
      end

      def get(nil, key) do
        get(key)
      end

      def get(current_value, _key) do
        current_value
      end
    end
  end
end
