defmodule FluxUtils.Env do
  def get_env(key, default, type \\ :string) do
    key =
      if is_atom(key) do
        key |> Atom.to_string() |> String.upcase()
      else
        key
      end

    if type == :boolean do
      case convert_to(System.get_env(key), type) do
        nil -> default
        value -> value
      end
    else
      convert_to(System.get_env(key), type) || default
    end
  end

  defp convert_to(nil, _type) do
    nil
  end

  defp convert_to(value, :string) do
    value
  end

  defp convert_to(value, :atom) do
    try do
      String.to_atom(value)
    rescue
      _error -> nil
    end
  end

  defp convert_to(value, :boolean) do
    try do
      String.to_existing_atom(value)
    rescue
      _error -> nil
    end
  end

  defp convert_to(value, :float) do
    try do
      String.to_float(value)
    rescue
      _error -> nil
    end
  end

  defp convert_to(value, :integer) do
    try do
      String.to_integer(value)
    rescue
      _error -> nil
    end
  end

  defp convert_to(value, {:list, divisor}) do
    try do
      String.split(value, divisor)
    rescue
      _error -> nil
    end
  end

  defp convert_to(_value, _type) do
    nil
  end
end
