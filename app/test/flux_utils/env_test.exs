defmodule FluxUtils.EnvTest do
  use ExUnit.Case, async: true

  describe "get_env/3" do
    alias FluxUtils.Env

    test "returns env value if it is defined" do
      System.put_env("TEST_GET_ENV", "MEOW")
      assert Env.get_env("TEST_GET_ENV", "WOOF") == "MEOW"
    end

    test "returns default value if env is not defined" do
      System.delete_env("TEST_GET_ENV")
      assert Env.get_env("TEST_GET_ENV", "WOOF") == "WOOF"
    end

    test "works as expected with key as atom" do
      System.put_env("TEST_GET_ENV", "MEOW")
      assert Env.get_env(:test_get_env, "WOOF") == "MEOW"

      System.delete_env("TEST_GET_ENV")
      assert Env.get_env(:test_get_env, "WOOF") == "WOOF"
    end

    test "works as expected with conversions" do
      System.put_env("ATOM", "meow")
      assert Env.get_env("ATOM", nil, :atom) == :meow
      System.put_env("ATOM", "Warning: this function creates atoms dynamically and atoms are not garbage-collected. Therefore, string should not be an untrusted value, such as input received from a socket or during a web request. Consider using to_existing_atom/1 instead. The maximum atom size is of 255 Unicode codepoints.")
      assert Env.get_env("ATOM", :ok, :atom) == :ok
      System.delete_env("ATOM")

      System.put_env("BOOLEAN", "true")
      assert Env.get_env("BOOLEAN", nil, :boolean) == true
      System.put_env("BOOLEAN", "non_boolean")
      assert Env.get_env("BOOLEAN", true, :boolean) == true
      System.delete_env("BOOLEAN")

      System.put_env("FLOAT", "3.14")
      assert Env.get_env("FLOAT", nil, :float) == 3.14
      System.put_env("FLOAT", "non_float")
      assert Env.get_env("FLOAT", 3.14, :float) == 3.14
      System.delete_env("FLOAT")

      System.put_env("INTEGER", "-42")
      assert Env.get_env("INTEGER", nil, :integer) == -42
      System.put_env("INTEGER", "non_integer")
      assert Env.get_env("INTEGER", -42, :integer) == -42
      System.delete_env("INTEGER")

      System.put_env("LIST", "first,second,third")
      assert Env.get_env("LIST", nil, {:list, ","}) == ["first", "second", "third"]
      assert Env.get_env("LIST", ["first", "second", "third"], {:list, nil}) == ["first", "second", "third"]
      System.delete_env("LIST")
    end
  end
end
